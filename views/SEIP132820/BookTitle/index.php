<?php
require_once("../../../vendor/autoload.php");

use App\book_title\BookTitle;

$objBookTitle= new BookTitle();
$allData = $objBookTitle->index();
/*foreach($allData as $oneData)
{
   echo $oneData->id."<br>";
   echo $oneData->book_title."<br>";
    echo $oneData->author_name."<br>";
}*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Book Title</h2>
    <table class="table table-bordered">

        <thead>
        <tr>
            <th>Id</th>
            <th>Book Title</th>
            <th>Author Name</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($allData as $oneData)
        {
        ?>
            <tr >
                <td ><?php echo $oneData['id']; ?></td >
                <td ><?php echo $oneData['book_title']; ?></td >
                <td ><?php echo $oneData['author_name']; ?></td >
            </tr >
        <?php
        }
        ?>
        </tbody>
    </table>
</div>

</body>
</html>

<?php
namespace App\book_title;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{   public $id="";
    public $book_title="";
    public $author_name="";
    public function __construct()
    {
        parent::__construct();
    }
    //here 'book_name','author_name' are the name field of the form
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists('book_name',$data))
        {
            $this->book_title=$data['book_name'];
        }
        if(array_key_exists('author_name',$data))
        {
            $this->author_name=$data['author_name'];
        }
    }
    public function store()
    {
        $arr= array($this->book_title,$this->author_name);
        $sql="insert into book_title (book_title,author_name) values (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arr);
        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name]        <br> Success! Data Has Been Inserted Successfully!</h3></div>");
        else
            Message::message("<div id='msg'></div><h3 align='center'>[ BookTitle: $this->book_title ] , [ AuthorName: $this->author_name]        <br> Failed! Data Has Not Been Inserted Successfully!</h3></div>");
        Utility::redirect('create.php');
    }

    public function index($mode="ASSOC"){

        $mode=strtoupper($mode);
        $STH = $this->DBH->query('SELECT * from book_title');
        if($mode=="OBJ")
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData  = $STH->fetchAll();
        return $arrAllData;
    }

}